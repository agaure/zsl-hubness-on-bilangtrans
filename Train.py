import cPickle
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LinearRegression
from sklearn.decomposition import  PCA
from sklearn.metrics import accuracy_score
from matplotlib import pyplot as plt
import numpy as np

f = open("EN-HI.pkl", 'rb')
WordsEN, EmbedEN, WordsHI, EmbedHI = cPickle.load(f)
f.close()
EmbedENTrain = EmbedEN[0 : 2000]
WordsENTrain = WordsEN[0 : 2000]
EmbedHITrain = EmbedHI[0 : 2000]
WordsHITrain = WordsHI[0 : 2000]

EmbedENTest = EmbedEN[2000 : ]
WordsENTest = WordsEN[2000 : ]
EmbedHITest = EmbedHI[2000 : ]
WordsHITest = WordsHI[2000 : ]


LRigress = LinearRegression()
LRigress.fit(EmbedENTrain, EmbedHITrain)

Mapped = LRigress.predict(EmbedENTest)

neigh = KNeighborsClassifier()
neigh.fit(EmbedHI, WordsHI)
Predictions = neigh.predict(Mapped)
print "ACC:", accuracy_score(Predictions, WordsHITest)
#for i,x in enumerate(zip(Predictions, WordsHITest)):
#    if x[0] == x[1]:
#        print x[0], WordsENTest[i]

'''
for i, j in enumerate(WordsEN):
    if j == "july":
        print i

pme = EmbedHI
pca = PCA(n_components = 2)
X = pca.fit_transform(pme)
#print type(X), type(X[0]), len(X[0])
Xs = [p[0] for p in X]
Ys = [p[1] for p in X]
plt.plot(Xs, Ys,'o')
for i, w in enumerate(WordsEN):
    if w in ["anger", "atheist", "revenge", "june", "july", "november", "april", "dollar", "wallet" ] :
        plt.annotate(w, xy= (Xs[i], Ys[i]), bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5) )
    plt.annotate(w, xy= (Xs[i], Ys[i]))
plt.show()


neigh = KNeighborsClassifier(n_neighbors = 5)
neigh.fit(EmbedHI, WordsHI)
x = 2195
print "Word:",WordsHI[x]
w, i = neigh.kneighbors(EmbedHI[x])
for im in i[0]:
    print WordsHI[im]

'''

import cPickle
import codecs
import numpy

with codecs.open("en_hi.txt", 'r', 'utf8') as f:
    data = f.readlines()
f.close()


f = open("polyglot-en.pkl", 'r')
wordsEN, embedEN = cPickle.load(f)
f.close()

f = open("polyglot-hi.pkl", 'r')
wordsHI, embedHI = cPickle.load(f)
f.close()


ENtoHI = [line.split('\t') for line in data]
#print len(ENtoHI)

#print ENtoHI[80][1], wordsHI[80].strip('\n')

c = 0
WordDataEN, WordDataHI = [], []
EmbedDataEN, EmbedDataHI = [], []
for ENword, embedEN1 in zip(wordsEN, embedEN):
    Wdict = [x[1] for x in ENtoHI if x[0] == ENword]
    if Wdict :
        Found = [(x, embedHI1) for x, embedHI1 in zip(wordsHI, embedHI) if Wdict[0].strip('\n') == x]
        if Found:
            WordDataEN.append(ENword)
            EmbedDataEN.append(embedEN1)
            WordDataHI.append(Found[0][0])
            EmbedDataHI.append(Found[0][1])
            
FullData = (WordDataEN, EmbedDataEN, WordDataHI, EmbedDataHI)
            
fhandle = open("EN-HI.pkl",'wb')
cPickle.dump(FullData, fhandle)
fhandle.close()


            
            
        #print [x[1] for x in ENtoHI if x[1] == wordsHI[80]]

'''
#for HIWord in wordsHI:
    Found = [x for x in ENtoHI if x[1].strip('\n') == HIWord]
    if Found:
            c+=1
'''
